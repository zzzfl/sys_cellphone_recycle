package com.qf.controller;

import com.qf.entity.Good;
import com.qf.entity.Info;
import com.qf.entity.vo.ResultVO;
import com.qf.service.EvlService;
import com.qf.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 时间：2021/7/16 21:08
 * 我无敌，你随意
 */
@Controller
public class EvlController {

    @Autowired
    private GoodService goodService;

    @Autowired
    private EvlService evlService;


    @RequestMapping("evl")
    public String info(String goodId, Model model,String basicInfoId, String[] evlDetailIds){

        Good good = goodService.selectGoodById(goodId);
        model.addAttribute("good",good);

//        List<Info> infoList1 = evlService.selectAllInfoByType("1");
//        model.addAttribute("infoList1",infoList1);
//
//        List<Info> infoList2 = evlService.selectAllInfoByType("2");
//        model.addAttribute("infoList2",infoList2);
//
//        List<Info> infoList3 = evlService.selectAllInfoByType("3");
//        model.addAttribute("infoList3",infoList3);

        List<Info> infoList11 = evlService.selectAllInfo(goodId, "1");
        model.addAttribute("infoList11",infoList11);

        List<Info> infoList22 = evlService.selectAllInfo(goodId, "2");
        model.addAttribute("infoList22",infoList22);

        List<Info> infoList33 = evlService.selectAllInfo(goodId, "3");
        model.addAttribute("infoList33",infoList33);

        model.addAttribute("goodId",goodId);

        model.addAttribute("evlDetailIds",evlDetailIds);

//        List<Detail> detailList = evlService.selectDetailByInfoId(goodId, "1");
//        model.addAttribute("detailList",detailList);


//        String bii = null;
//        List<Info> infoList = evlService.selectAllInfo();
//        for (int i = 0; i < infoList.toArray().length; i++) {
//            bii = infoList.get(i).getBasicInfoId()+"";
//            basicInfoId = bii;
//            List<Detail> detailList = evlService.selectDetailByInfoId(goodId, basicInfoId);
//            model.addAttribute("detailList",detailList);
//        }

        return "evl";
    }

    @RequestMapping("findAllEvl")
    @ResponseBody
    public List<Info> findAllEvl(String goodId){

        List<Info> infoList22 = evlService.selectAllInfo(goodId, "2");

        return infoList22;
    }

    @RequestMapping("price")
    public String doEvl( String[] evlDetailIds,  String goodId,Model model){

        Integer evlPrice = evlService.doEvl(goodId, evlDetailIds);
        Good good = goodService.selectGoodById(goodId);
        model.addAttribute("good",good);

//        Object data = resultVO.getData();
        model.addAttribute("evlPrice",evlPrice);

        return "price";

    }

//    @RequestMapping("result")
//    public String result(){
//
//        return "price";
//    }
}
