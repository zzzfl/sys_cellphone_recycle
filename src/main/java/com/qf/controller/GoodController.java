package com.qf.controller;

import com.github.pagehelper.PageInfo;
import com.qf.entity.Brand;
import com.qf.entity.Category;
import com.qf.entity.Good;
import com.qf.service.BrandService;
import com.qf.service.CategoryService;
import com.qf.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 时间：2021/7/15 11:35
 * 我无敌，你随意
 */
@Controller
public class GoodController {

    @Autowired
    private GoodService goodService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private BrandService brandService;


    @RequestMapping("index")
    public String index(Model model, String catId, String brandId
            , @RequestParam(required = false, defaultValue = "1") Integer page
            , @RequestParam(required = false, defaultValue = "2") Integer size) {

        String categoryId = null;

        //获取所有的分类信息
        List<Category> categoryList = categoryService.selectAllCategory();
        model.addAttribute("categoryList", categoryList);

        if (catId == null) {
            categoryId = categoryList.get(0).getCategoryId() + "";
        } else {
            categoryId = catId;
        }

        //默认分类信息
        model.addAttribute("categoryId", categoryId);

        //获取分类的品牌信息
        List<Brand> brandList = brandService.selectBrandByCategoryId(categoryId);
        model.addAttribute("brandList", brandList);

        PageInfo<Good> pageInfo = goodService.selectGoodsByWhereAndPage(categoryId, brandId, page, size);
        model.addAttribute("goodList", pageInfo.getList());//当前页数据
        model.addAttribute("pages", pageInfo.getPages());//总页数
        model.addAttribute("total", pageInfo.getTotal());//总记录数
        model.addAttribute("brandId", brandId);
        model.addAttribute("page", page);//当前页

        return "goods";
    }


}
