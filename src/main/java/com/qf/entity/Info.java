package com.qf.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * 时间：2021/7/17 11:55
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Info {

    private Integer basicInfoId;
    private String basicInfoName; //手机基本信息
    private Integer basicInfoType;
    private Integer multiple;
    private Detail detail;

    private List<Detail> detailList;
}
