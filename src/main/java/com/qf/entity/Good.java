package com.qf.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 时间：2021/7/15 10:33
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Good {

    private Integer categoryId;
    private String categoryName;

    private Integer brandId;
    private String brandName;

    private Integer goodId;
    private String goodName;
    private String goodImg;
    private Integer goodBrandId;
    private Integer goodCost;
    private Integer goodMinPrice;
    private Integer goodFirstPrice;
    private Integer goodSecondPrice;
    private Integer goodThirdPrice;
    private Integer goodForthPrice;

    private List<Discount> discountList;

}
