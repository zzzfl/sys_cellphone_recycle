package com.qf.entity.vo;

import lombok.*;

/**
 * 时间：2021/7/19 14:16
 * 我无敌，你随意   事务操作  增删改
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class ResultVO {

    @NonNull
    private boolean success;  //标记操作的状态

    @NonNull
    private String msg;  //错误信息

    private Object data;  //数据
}
