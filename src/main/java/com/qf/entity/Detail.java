package com.qf.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 时间：2021/7/17 11:53
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Detail {   //基本信息里的选项/细节

    private Integer infoDetailId;
    private String infoDetailName;  //选项名字
    private String infoDetailDesc;  //选项介绍
    private Integer fkBasicInfoId;

}
