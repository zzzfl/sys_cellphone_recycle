package com.qf.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 时间：2021/7/17 11:51
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Discount {

    private Integer gdId;
    private Integer fkGoodId;
    private Integer fkInfoDetailId;
    private Integer goodDiscount;

    private Detail detail;
}
