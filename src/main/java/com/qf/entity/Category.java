package com.qf.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 时间：2021/7/15 10:30
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Category {     //数码类别
    private Integer categoryId;
    private String categoryName;
}
