package com.qf.service;

import com.qf.entity.Brand;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 时间：2021/7/16 11:57
 * 我无敌，你随意
 */
public interface BrandService {

    public List<Brand> selectBrandByCategoryId(String categoryId);
}
