package com.qf.service;

import com.qf.entity.Detail;
import com.qf.entity.Info;

import java.util.List;

/**
 * 时间：2021/7/17 17:24
 * 我无敌，你随意
 */
public interface EvlService {

    public List<Info> selectAllInfo(String goodId, String basicInfoType);

    public List<Info> selectAllInfoByType(String basicInfoType);

    public List<Detail> selectDetailByInfoId(String goodId, String basicInfoId);

    //估价
    public Integer doEvl(String goodId, String[] evlDetailIds);

}
