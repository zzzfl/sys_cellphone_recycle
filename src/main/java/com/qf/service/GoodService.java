package com.qf.service;

import com.github.pagehelper.PageInfo;
import com.qf.entity.Good;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 时间：2021/7/15 11:30
 * 我无敌，你随意
 */
public interface GoodService {

    public PageInfo<Good> selectGoodsByWhereAndPage(String catId, String brandId, Integer page, Integer size);

    public Good selectGoodById(String goodId);
}
