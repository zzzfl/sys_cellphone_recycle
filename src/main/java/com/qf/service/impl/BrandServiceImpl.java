package com.qf.service.impl;

import com.qf.dao.BrandDAO;
import com.qf.entity.Brand;
import com.qf.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 时间：2021/7/16 11:59
 * 我无敌，你随意
 */
@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandDAO brandDAO;


    @Override
    public List<Brand> selectBrandByCategoryId(String categoryId) {
        List<Brand> brandList = brandDAO.selectBrandByCategoryId(categoryId);
        return brandList;
    }
}
