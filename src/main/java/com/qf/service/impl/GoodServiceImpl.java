package com.qf.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qf.dao.GoodDAO;
import com.qf.entity.Good;
import com.qf.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 时间：2021/7/15 11:33
 * 我无敌，你随意
 */
@Service
public class GoodServiceImpl implements GoodService {

    @Autowired
    private GoodDAO goodDAO;

    @Override
    public PageInfo<Good> selectGoodsByWhereAndPage(String catId, String brandId, Integer page, Integer size) {

        //设置分页
        PageHelper.startPage(page, size);

        List<Good> goodList = goodDAO.findGoodsByWhereAndPage(catId, brandId);

        PageInfo<Good> pageInfo = new PageInfo<Good>(goodList);

        return pageInfo;
    }

    @Override
    public Good selectGoodById(String goodId) {
        return goodDAO.findGoodById(goodId);
    }
}
