package com.qf.service.impl;

import com.qf.dao.CategoryDAO;
import com.qf.entity.Category;
import com.qf.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 时间：2021/7/15 19:54
 * 我无敌，你随意
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDAO categoryDAO;


    @Override
    public List<Category> selectAllCategory() {
        List<Category> categoryList = categoryDAO.selectAllCategory();
        return categoryList;
    }
}
