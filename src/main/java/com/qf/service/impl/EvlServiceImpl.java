package com.qf.service.impl;

import com.qf.dao.DiscountDAO;
import com.qf.dao.EvlDAO;
import com.qf.dao.GoodDAO;
import com.qf.entity.Detail;
import com.qf.entity.Discount;
import com.qf.entity.Good;
import com.qf.entity.Info;
import com.qf.entity.vo.ResultVO;
import com.qf.service.EvlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 时间：2021/7/17 17:24
 * 我无敌，你随意
 */
@Service
public class EvlServiceImpl implements EvlService {

    @Autowired
    private EvlDAO evlDAO;

    @Autowired
    private GoodDAO goodDAO;

    @Autowired
    private DiscountDAO discountDAO;

    @Override
    public List<Info> selectAllInfo(String goodId, String basicInfoType) {
        return evlDAO.selectAllInfo(goodId,basicInfoType);
    }

    @Override
    public List<Info> selectAllInfoByType(String basicInfoType) {

        return evlDAO.selectAllInfoByType(basicInfoType);
    }

    @Override
    public List<Detail> selectDetailByInfoId(String goodId, String basicInfoId) {
        return evlDAO.selectDetailByInfoId(goodId,basicInfoId);
    }

    @Override
    public Integer doEvl(String goodId, String[] evlDetailIds) {

//        if (evlDetailIds == null || evlDetailIds.length <= 0 || goodId == null){
//            System.out.println(evlDetailIds);
//            System.out.println(goodId);
//            return new ResultVO( false ,"参数不合法");
//        }

        //获取商品信息
        Good good = goodDAO.findGoodById(goodId);

        //获取明细的折扣金额数据
        List<Discount> discountList = discountDAO.findEvlDetailDisCount(goodId, evlDetailIds);

        Integer goodCost = good.getGoodCost();
        for (Discount discount : discountList){

            goodCost -= Integer.parseInt(String.valueOf(discount.getGoodDiscount()));
        }
        System.out.println(goodCost);
//        //判断价格是否低于最低价格
//        if (goodCost <= good.getGoodMinPrice()){
//            return new ResultVO(false , "价格太低,不建议回收");
//        }

//        System.out.println(goodCost);
//        Integer finalGoodCost = goodCost;
//        Map data = new HashMap(){{
//            put("good",good);
//            put("price",finalGoodCost);
//            put("evlDetails",discountList);
//        }};

        return goodCost;
//        return new ResultVO(true, "success",data);
    }
}
