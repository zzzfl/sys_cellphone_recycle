package com.qf.service;

import com.qf.entity.Category;

import java.util.List;

/**
 * 时间：2021/7/15 19:51
 * 我无敌，你随意
 */


public interface CategoryService {

    public List<Category> selectAllCategory();
}