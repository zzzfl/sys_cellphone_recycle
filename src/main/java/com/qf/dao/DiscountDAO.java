package com.qf.dao;

import com.qf.entity.Discount;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 时间：2021/7/19 20:40
 * 我无敌，你随意
 */
public interface DiscountDAO {

    public List<Discount> findEvlDetailDisCount(@Param("goodId") String goodId,
                                                @Param("evlDetailIds") String[] evlDetailIds);
}
