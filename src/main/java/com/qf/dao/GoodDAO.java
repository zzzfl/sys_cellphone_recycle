package com.qf.dao;

import com.qf.entity.Good;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 时间：2021/7/15 10:39
 * 我无敌，你随意
 */
public interface GoodDAO {

    public List<Good> findGoodsByWhereAndPage(@Param("categoryId") String categoryId,
                                                @Param("brandId") String brandId);

    public Good findGoodById(String goodId);


}
