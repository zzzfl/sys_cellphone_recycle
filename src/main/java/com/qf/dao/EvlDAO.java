package com.qf.dao;

import com.qf.entity.Detail;
import com.qf.entity.Info;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 时间：2021/7/17 14:09
 * 我无敌，你随意
 */
public interface EvlDAO {

    public List<Info> selectAllInfo(@Param("goodId") String goodId,
                                    @Param("basicInfoType") String basicInfoType);

    public List<Info> selectAllInfoByType(String basicInfoType);

    public List<Detail> selectDetailByInfoId(@Param("goodId") String goodId,
                                             @Param("basicInfoId") String basicInfoId);

}
