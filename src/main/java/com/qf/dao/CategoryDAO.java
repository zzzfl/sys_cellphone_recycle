package com.qf.dao;

import com.qf.entity.Category;

import java.util.List;

/**
 * 时间：2021/7/15 10:37
 * 我无敌，你随意
 */
public interface CategoryDAO {
    public List<Category> selectAllCategory();
}
