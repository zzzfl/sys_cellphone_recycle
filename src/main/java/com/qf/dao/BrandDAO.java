package com.qf.dao;

import com.qf.entity.Brand;

import java.util.List;

/**
 * 时间：2021/7/15 10:36
 * 我无敌，你随意
 */
public interface BrandDAO {

    public List<Brand> selectBrandByCategoryId(String categoryId);
}
