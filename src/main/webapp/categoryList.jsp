<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Title</title>
</head>

<body>
<table width="80%" align="center" border="1px" cellpadding="10px" cellspacing="0px" bgcolor="#f5f5dc">
    <tr>
        <th>品牌id</th>
        <th>品牌名字</th>
    </tr>
    <c:forEach items="${categoryList}" var="category">
        <tr>
            <td>${category.categoryId}</td>
            <td>${category.categoryName}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>