<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>手机回购明细</title>
    <meta name="keywords" content="iphone 7 iphone7 7 苹果7 苹果 7"/>
    <meta name="description" content="iphone 7 iphone7 7 苹果7 苹果 7"/>
    <meta name="baidu-site-verification" content="IpuQLFxZiC"/>
    <meta name="chinaz-site-verification" content="081e7651-48c6-4c2f-a569-99321685eab1"/>
    <meta name="mobile-agent" content="format=html5;url=http://m.ihuigo.com/goods/info/21025.html">
    <meta name="applicable-device" content="pc">
    <link rel="alternate" media="only screen and(max-width: 640px)" href="http://m.ihuigo.com/goods/info/21025.html">
    <link href="favicon.ico" type=image/x-icon rel="Icon">
    <link href="css/new_css/global.css" rel="stylesheet" type="text/css">
    <script src="js/new_js/jquery-1.4.2.js" type="text/javascript"></script>
<%--    <script src="js/new_js/new_public.js" type="text/javascript"></script>--%>
    <script type="text/javascript"></script>
    <link href="css/new_css/sub.css" rel="stylesheet" type="text/css">
    <script src="js/public.js" type="text/javascript"></script>
    <script src="js/new_js/menu.js" type="text/javascript"></script>
<%--    <script src="js/new_js/new_goods.js" type="text/javascript"></script>--%>
</head>
<body>
<link rel="stylesheet" type="text/css" href="css/alert_zhe.css"/>
<script type="text/javascript" src="js/alert_zhe.js"></script>
<!--头部--><!--手机回收-->
<div class="recovery">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".new_search_btn").click(function () {
                $(".new_search").slideToggle();
            });
        });
    </script>
    <div class="page2" style="position:relative">
        <div class="new_search_btn" style="position:absolute; left:-40px; top:-11px; cursor:pointer;"><img
                src="images/new_images/search_btn.png" alt="搜索您要评估价格的机型"/></div>

        <div style="height:30px; line-height:30px; font-size:14px;">
            <a href="${pageContext.request.contextPath}/index">首页</a> &gt;
            ${good.goodName}
        </div>
        <div class="heat_l">
            <div class="heat_img">
                <img src="${pageContext.request.contextPath}/images/${good.goodImg}" alt="${good.goodName}回收价格评估"
                     width="264" height="197"/>
            </div>
            <div class="heat_txt">
                <div class="heat_name"><span>${good.goodName}</span></div>
                <dl class="heat_ts">
                    <dd><em>${good.goodCost}</em>元<br/>
                        30天内回收最高价
                    </dd>
                    <dt class="clear"></dt>
                </dl>
                <div class="heat_bao"><img src="images/new_images/icon10.gif" alt="iphone 7回收免费享受第三方理赔服务"/>可免费享受第三方理赔服务
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <script src="js/new_js/morris/raphael-2.1.0.min.js"></script>
        <script src="js/new_js/morris/morris.js"></script>
        <link rel="stylesheet" href="js/new_js/morris/morris.css">
        <div class="heat_r">
            <div class="heat_title"><span>回购价格趋势</span><samp>单位：元</samp></div>
            <div class="heat_pic">
                <div id="graph" style="width:360px; height:210px; margin-top:0px;"></div>
            </div>
            <script>
                var day_data = [
                    {"elapsed": "7", "价格": "2614"}
                    , {"elapsed": "8", "价格": "2607"}
                    , {"elapsed": "9", "价格": "2660"}
                    , {"elapsed": "10", "价格": "2822"}
                    , {"elapsed": "11", "价格": "2929"}
                ];
                Morris.Line({
                    element: 'graph',
                    data: day_data,
                    xkey: 'elapsed',
                    ykeys: ['价格'],
                    labels: ['价格'],
                    parseTime: false
                });
            </script>
        </div>
        <div class="clear"></div>
    </div>
</div>
<!--手机回收-->
<!--评估流程-->
<div id="ahs_trade_body">
    <input type="hidden" id="view" value="goods_info"/>
    <input type="hidden" id="if_show_price" value="1"/>
    <input type="hidden" id="gid" name="gid" value="21025"/>
    <input type="hidden" id="package_id" name="package_id" value="571"/>
    <div id="ahs_property_head">
        <p>开始评估你的${good.goodName}，只需3步，获得产品精准评估报价</p>
        <!--步骤一-->
        <div class="speed" id="speed1"><img src="images/new_images/step1.gif" alt="第1步评估手机的基本情况"/></div>
        <!--步骤二-->
        <div class="speed" id="speed2"><img src="images/new_images/step2.gif" alt="第2步评估手机的功能使用情况"/></div>
        <!--步骤三-->
        <div class="speed" id="speed3"><img src="images/new_images/step3.gif" alt="第3步评估手机的外观成色"/></div>
    </div>

    <div id="ahs_property_body">

        <script>

            //第一步 评估项的点击事件
            function item_click(type,evlId,evlDetailId,target){

                //将评估项下面所有的评估明细的selected样式去掉
                $("#property_step1 #info11"+evlId+" li").attr("class","");

                //将点击的评估明细的selected样式加上
                $(target).attr("class","selected");

                //将点击的评估明细的id赋值给对应input
                $("#property_step1 #info11"+evlId+" input").val(evlDetailId);
            }



            $(function (){

                //第一步的下一步
                $("#btn_step1_next").click(function (){
                    //获取第一步所有的input
                    var  evlFinished = true;
                    $("#property_step1 input").each(function (){
                        if ($(this).val() == "0"){
                            evlFinished = false;
                        }
                    })
                    if (evlFinished){
                        //第二步 显示和隐藏
                        $("#property_step2").show();
                        $("#property_step1").hide();
                        $("#speed2").show();
                        $("#speed1").hide();
                    }else {
                        alert("请完成所有评估项")
                    }
                })

                //第二步的返回上一步
                $("#btn_step2_back").click(function (){
                    $("#property_step1").show();
                    $("#property_step2").hide();
                    $("#speed1").show();
                    $("#speed2").hide();
                })

                //第二步的下一步
                $("#btn_step2_next").click(function (){

                    var isfinished = true;

                    //异步请求所有的评估项
                    $.get("${pageContext.request.contextPath}/findAllEvl?goodId=${goodId}",function (res){

                        //获取第二步所有的评估项
                        $.each(res,function (index,item){
                            if (item.multiple != "1"){
                                //获取input
                                var val = $("#property_step2 #info22"+item.basicInfoId+" input").val();
                                if(val == "0"){
                                    isfinished = false;
                                }
                            }
                        })
                        if (isfinished){
                            //第三步 显示和隐藏
                            $("#property_step3").show();
                            $("#property_step2").hide();
                            $("#speed3").show();
                            $("#speed2").hide();
                        }else {
                            alert("请完成所有评估项")
                        }
                    },"json")
                })

                //第三步的返回上一步
                $("#btn_step3_back").click(function (){
                    $("#property_step2").show();
                    $("#property_step3").hide();
                    $("#speed2").show();
                    $("#speed3").hide();
                })

                //第三步的下一步
                $("#btn_step3_next").click(function (){
                    //获取第一步所有的input
                    var  evlFinished = true;
                    $("#property_step3 input").each(function (){
                        if ($(this).val() == "0"){
                            evlFinished = false;
                        }
                    })
                    if (evlFinished){

                        //提交用户勾选数据进行价格评估

                        var arr = [];

                        $("input[name='property[]']").each(function (){
                            var item = $(this).val();
                            if (item != "0"){
                                arr.push(item);
                            }
                        })
                        // window.location.href = "good/calc?subTypeIds=" + subTyIds + "&goodId=" + phone_id;
                        location.href = "price?evlDetailIds=" + arr + "&goodId=${goodId}"

                    }else {
                        alert("请完成所有评估项")
                    }
                })
            })

            //第二步的下一步
            function item_click_2(type,evlId,evlDetailId,target,multiple){

                //将评估项下面所有的评估明细的selected样式去掉
                if (multiple != "1"){//单选
                    $("#property_step2 #info22"+evlId+" li").attr("class","");
                    $("#property_step2 #info22"+evlId+" input").val(evlDetailId);
                }else {//多选
                    //获取点击的明细对应的input
                    if($(target).attr("class") == "selected"){
                        $("#input"+evlDetailId).val(0);
                    }else {
                        $("#input"+evlDetailId).val(evlDetailId);
                    }
                }

                //将点击的评估明细的selected样式加上
                $(target).toggleClass("selected")

                //将点击的评估明细的id赋值给对应input
                $("#property_step1 #info11"+evlId+" input").val(evlDetailId);
            }


            //第三步 评估项的点击事件
            function item_click_3(type,evlId,evlDetailId,target){

                //将评估项下面所有的评估明细的selected样式去掉
                $("#property_step3 #outlook"+evlId+" li").attr("class","");

                //将点击的评估明细的selected样式加上
                $(target).attr("class","selected");

                //将点击的评估明细的id赋值给对应input
                $("#property_step3 #outlook"+evlId+" input").val(evlDetailId);
            }


        </script>


        <!--第一步-->
        <div id="property_step1">
            <dl>
               <c:forEach items="${infoList11}" var="info11">
                   <dd id="info11${info11.basicInfoId}">
                       <input type="hidden" name="property[]" id="property_version" value="0"/>
                       <div class="property_title"><h3>${info11.basicInfoName}</h3></div>
                       <ul>
                           <c:forEach items="${info11.detailList}" var="detail">
                               <li onclick="item_click('info11',${info11.basicInfoId},${detail.infoDetailId},this)" name="sx_child_1">
                                   <div class="pro_div">
                                       <span class="property_value"><i>${detail.infoDetailName}</i><ins>${detail.infoDetailDesc}</ins></span>
                                       <span class="gou"></span>
                                   </div>
                               </li>
                           </c:forEach>
                           <div class="clear"></div>
                       </ul>
                   </dd>
               </c:forEach>
                <div id="step1_nav" class="property_nav">
                    <div id="btn_step1_next" class="btn_next">下一步</div>
                </div>
            </dl>
        </div>



        <!--第一步-->
        <!--第二步-->
        <div id="property_step2" class="hide">
            <dl>
                <c:forEach items="${infoList22}" var="info22">
                    <dd id="func${info22.basicInfoId}">
                        <div class="property_title"><h3>${info22.basicInfoName}</h3></div>
                        <ul>
                            <c:if test="${info22.multiple!='1'}">
                                <input type="hidden" name="property[]"  value="0"/>
                            </c:if>

                            <c:forEach items="${info22.detailList}" var="detail">
                                <c:if test="${info22.multiple=='1'}">
                                    <input id="input${detail.infoDetailId}" type="hidden" name="property[]"  value="0"/>
                                </c:if>
                                <li onclick="item_click_2('func',${info22.basicInfoId},${detail.infoDetailId},this,${info22.multiple})" name="mx_child_944">
                                    <div class="pro_div">
                                        <span class="property_value"><i>${detail.infoDetailName}</i><ins>${detail.infoDetailDesc}</ins></span>
                                        <span class="gou"></span>
                                    </div>
                                </li>
                            </c:forEach>
                            <div class="clear"></div>
                        </ul>
                    </dd>
                </c:forEach>
            </dl>
            <div id="step2_nav" class="property_nav">
                <div id="btn_step2_back" class="btn_back">< 返回上一步</div>
                <div id="btn_step2_next" class="btn_next">下一步 ></div>
            </div>
        </div>
        <!--第二步-->
        <!--第三步-->
        <div id="property_step3" class="hide">
            <dl>
               <c:forEach items="${infoList33}" var="info33">
                   <dd class="" id="outlook${info33.basicInfoId}">
                       <input type="hidden" name="property[]" value="0"/>
                       <div class="property_title"><h3>${info33.basicInfoName}</h3></div>
                       <ul>
                           <c:forEach items="${info33.detailList}" var="detail">
                               <li class="" onclick="item_click_3('outlook',${info33.basicInfoId},${detail.infoDetailId},this)" name="mx_child_659">
                                   <div class="pro_div">
                                       <span class="property_value"><i>${detail.infoDetailName}</i><ins>${detail.infoDetailDesc}</ins></span>
                                       <span  class="gou"></span>
                                   </div>
                               </li>
                           </c:forEach>
                           <div class="clear"></div>
                       </ul>
                   </dd>
               </c:forEach>
            </dl>
            <div id="step3_nav" class="property_nav">
                <div id="btn_step3_back" class="btn_back">< 返回上一步</div>
                <div id="btn_step3_next" class="btn_next">查看价格 >
                </div>
            </div>
        </div>
        <!--第三步-->
    </div>
</div>
<!--评估流程-->
<!--隐藏的评估描叙选项start-->
<div style="display:none; background-color:#CCCCCC;">
</div>
<!--隐藏的评估描叙选项end-->
<script>
    $(document).ready(function () {
        // $("#step1_nav .btn_next").bind('click', step1_next);
        // $("#step2_nav .btn_next").bind('click', step2_next);
        // $("#step2_nav .btn_back").bind('click', step2_back);
        // $("#step3_nav .btn_back").bind('click', step3_back);
        //
        // $("input[name='property[]']").each(function () {
        //     $(this).val(0);
        // })
        // $("#property_step1 input[name='desc_id[]']").each(function () {
        //     $(this).val(0);
        // })
        // $("#property_step3 input[name='desc_id[]']").each(function () {
        //     $(this).val(0);
        // })
        // $("#pj_ids").val(0);
    })
</script>
<!--[if lte IE 6]>
<script src="../js/new_js/png.js" type="text/javascript"></script>
<script type="text/javascript">
    DD_belatedPNG.fix('img,.banner ul, li, dl, dt, dd, span, em, div, i, samp, a, b, .kuang');
</script>
<![endif]-->
</body>
</html>
